FROM alpine:latest as build 

RUN apk add openjdk8
RUN apk add gradle
RUN apk add nodejs
RUN apk add yarn

RUN mkdir /frontend
COPY ./dev-school-front-app-main /frontend
WORKDIR /frontend/
RUN gradle build

RUN ls -l /frontend/devschool-front-app-server/build/libs/devschool-front-app-server-1.0.0.jar
RUN echo "----=====++++++ FINNISH BUILD STAGE 1 ! +++++=====----"


FROM openjdk:11 as readyfrontend

RUN mkdir /app
COPY --from=build /frontend/devschool-front-app-server/build/libs/devschool-front-app-server-1.0.0.jar /app
RUN ls -l /app/
EXPOSE 80/tcp
EXPOSE 8080/tcp
EXPOSE 8081/tcp
ENTRYPOINT ["java","-jar","/app/devschool-front-app-server-1.0.0.jar","-port=80","-P:ktor.backend.port=8080","-P:ktor.backend.host=backend","-P:ktor.backend.schema=http"]

RUN echo "----=====++++++ FINNISH BUILD STAGE 2 ! +++++=====----"
